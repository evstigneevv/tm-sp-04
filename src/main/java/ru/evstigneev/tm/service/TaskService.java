package ru.evstigneev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.repository.ITaskRepository;
import ru.evstigneev.tm.util.DateParser;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@Service
@Transactional
public class TaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Transactional
    public Collection<Task> findAllByProject(@NotNull final String projectId) {
        if (projectId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return taskRepository.findAllByProject(projectId);
    }

    @Transactional
    public void create(@NotNull final Project project, @NotNull final String taskName,
                       @NotNull final String description) {
        if (taskName.isEmpty() || description.isEmpty()) {
            throw new IllegalArgumentException();
        }
        @NotNull final Task task = new Task();
        task.setId(UUID.randomUUID().toString());
        task.setProject(project);
        task.setName(taskName);
        task.setDescription(description);
        task.setDateOfCreation(new Date());
        task.setStatus(Status.PLANNING);
        taskRepository.save(task);
    }

    @Transactional
    public void persist(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Transactional
    public void remove(@NotNull final String taskId) throws Exception {
        if (taskId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        taskRepository.delete(findOne(taskId));
    }

    @Transactional
    public void update(@NotNull final String taskId, @NotNull final String name, @Nullable final String description,
                       @Nullable final String dateStart, @Nullable final String dateFinish,
                       @NotNull final Status status) throws Exception {
        if (taskId.isEmpty() || name.isEmpty() || description == null || description.isEmpty()) {
            throw new IllegalArgumentException();
        }
        taskRepository.update(taskId, name, description, DateParser.setDateByString(dateStart),
                DateParser.setDateByString(dateFinish), status);
    }

    @Transactional
    public Task findOne(@NotNull final String taskId) throws Exception {
        if (taskId.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return taskRepository.findById(taskId).orElse(null);
    }

    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

}
