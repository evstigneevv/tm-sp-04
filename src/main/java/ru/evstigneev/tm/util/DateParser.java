package ru.evstigneev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Named
@ViewScoped
public class DateParser {

    public static String getDateString(@Nullable final Date date) {
        if (date == null) return null;
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(date);
    }

    public static Date setDateByString(@Nullable final String dateString) throws ParseException {
        if (dateString == null || dateString.isEmpty()) return null;
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.parse(dateString);
    }

}