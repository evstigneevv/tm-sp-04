package ru.evstigneev.tm.managedbean;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;
import ru.evstigneev.tm.util.DateParser;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.Collection;
import java.util.Date;

@Named(value = "taskBean")
@ViewScoped
public class TaskBean {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;

    @NotNull
    private String id;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date dateOfCreation;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status;
    @NotNull
    private Project project;

    @Transactional
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Transactional
    public Collection<Task> findAllByProject(@NotNull final String projectId) {
        return taskService.findAllByProject(projectId);
    }

    @Transactional
    public String create(@NotNull final Project project, @NotNull final String taskName, @NotNull final String description) {
        taskService.create(project, taskName, description);
        return "/projectList?faces-redirect=true";
    }

    @Transactional
    public void persist(@NotNull final Task task) {
        taskService.persist(task);
    }

    @Transactional
    public String remove(@NotNull final String taskId) throws Exception {
        taskService.remove(taskId);
        return "/taskList?faces-redirect=true";
    }

    @Transactional
    public String update(@NotNull final String taskId, @NotNull final String name, @NotNull final String description,
                         @NotNull final Date dateStart, @NotNull final Date dateFinish, @NotNull final Status status) throws Exception {
        taskService.update(taskId, name, description, DateParser.getDateString(dateStart),
                DateParser.getDateString(dateFinish), status);
        return "/projectTaskList?faces-redirect=true";
    }

    @Transactional
    public Task findOne(@NotNull final String taskId) throws Exception {
        return taskService.findOne(taskId);
    }

    @Transactional
    public void removeAll() {
        taskService.removeAll();
    }

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(@NotNull final Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@NotNull final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@NotNull final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(@NotNull final Project project) {
        this.project = project;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(@NotNull final ProjectService projectService) {
        this.projectService = projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(@NotNull final TaskService taskService) {
        this.taskService = taskService;
    }

}
