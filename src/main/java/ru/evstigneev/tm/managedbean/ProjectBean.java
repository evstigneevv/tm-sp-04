package ru.evstigneev.tm.managedbean;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.util.DateParser;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Named(value = "projectBean")
@ViewScoped
public class ProjectBean {

    @Autowired
    private ProjectService projectService;

    @NotNull
    private String id;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date dateOfCreation;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status;

    @Nullable
    private List<Task> tasks;

    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(@NotNull final Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(@NotNull final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(@NotNull final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(@NotNull final ProjectService projectService) {
        this.projectService = projectService;
    }

    public Collection<Project> getProjects() {
        return projectService.findAll();
    }

    @Transactional
    public String create(@NotNull final String projectName, @NotNull final String description) {
        projectService.create(projectName, description);
        return "/projectList?faces-redirect=true";
    }

    @Transactional
    public String remove(@NotNull final String projectId) throws Exception {
        projectService.remove(projectId);
        return "/projectList?faces-redirect=true";
    }

    @Transactional
    public String update(@NotNull final String projectId, @NotNull final String name, @NotNull final String description,
                         @NotNull final Date dateStart, @NotNull final Date dateFinish, @NotNull final Status status) throws Exception {
        projectService.update(projectId, name, description, DateParser.getDateString(dateStart),
                DateParser.getDateString(dateFinish), status);
        return "/projectList?faces-redirect=true";
    }

    @Transactional
    public Project findOne(@NotNull final String projectId) throws Exception {
        return projectService.findOne(projectId);
    }

}
